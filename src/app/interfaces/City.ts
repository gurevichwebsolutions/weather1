export interface City extends Object {
  coord: {
    lat: number;
    lon: number;
  };
  country: string;
  id: number;
  name: string;
  population: number;
  sunrise: number;
  sunset: number;
  temperature: number;
  description: string;
  icon: string;
}
