import {Component, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { City } from '../../interfaces/City';
import { map, startWith } from 'rxjs/operators';
import cities from '/src/assets/data/cities.json';
import { HttpService } from '../../services/http.service';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cityform',
  templateUrl: './cityform.component.html',
  styleUrls: ['./cityform.component.scss']
})
export class CityformComponent implements OnInit {

  @Input() activeCities;

  @Output() newCityEvent = new EventEmitter<any>();

  constructor(private http: HttpService) { }

  public cityControl = new FormControl();

  public filteredCities: Observable<City[]>;

  public cities: any[] = cities;

  public currentCity: City;

  ngOnInit(): void {
    if (!this.currentCity) {
      this.getWeatherForCurrentCity();
    }

    this.filteredCities = this.cityControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.cities.slice())
      );
  }

  public getWeatherForCurrentCity = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.http.getCityByCoords(position).subscribe(data => {
          this.currentCity = data.name;
          this.getWeatherByCityName(data.name);
        });
      });
    }else{
      console.log('User not allowed');
    }
  }

  public getWeatherByCityName = (cityName: string = this.cityControl.value.name) => {
    if (!this.cityControl.value && !cityName) {
      return;
    }
    if (!(this.activeCities.filter(e => e.name === cityName).length > 0)) {
      this.http.getWeatherByCityName(cityName).subscribe(res => {
        this.activeCities.push(res);
      });
      this.newCityEvent.emit(this.activeCities);
    }
  }

  public displayFn(city: any): string {
    return city && city.name ? city.name : '';
  }

  private _filter(name: string): City[] {
    const filterValue = name.toLowerCase();
    return this.cities.filter(option => option.name.toLowerCase().includes(filterValue));
  }

}
