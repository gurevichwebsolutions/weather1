import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpService } from '../../services/http.service';
import cities from '/src/assets/data/cities.json';
import { City } from '../../interfaces/City';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(private http: HttpService) { }

  public activeCities: City[] = [];

  ngOnInit(): void {
  }

  public deleteCityWeather = (cityName) => {
    this.activeCities = this.activeCities.filter(el => !(el.name === cityName));
  }

  public refreshCityWeather = (cityName) => {
    this.http.getWeatherByCityName(cityName).subscribe(res => {
      this.activeCities.map((el, i) => {
        if (el.name === cityName) {
          this.activeCities[i] = res;
        }
      });
    });
  }
}
